'use strict';

describe('Directive: matrixcharter', function () {

  var templater = '<div><div class="matrixcharter_area"><div class="id_area">{{chartdata.id}}<span ng-if="chartdata.sid != 0">-{{chartdata.sid}}</span></div><div class="matrix_area"><div ng-repeat="row in matrix track by $index" class="one-row"><span ng-repeat="item in row track by $index" class="item" style="background-color: {{colorswatch[item.color]}}" title="{{item.valuer +\' \'+ timedenom.keyer}}"></span></div></div><div class="stats_area"><div class="percent_area">{{chartdata.percentage}}%</div><div class="count_area">{{chartdata.count}} UIDs</div></div></div><div class="legend_area" ng-show="{{showlegend}}"><div class="legend_title_area"><p>Time ({{timedenom.keyer}})</p></div><div class="legend_colors_area"><div class="item" ng-repeat="coloritem in colorswatch" style="background-color: {{coloritem}}"></div></div><div class="legend_values_area"><div>0</div><div>{{timedenom.valuer}}</div></div></div></div>';

  var scopeMatrixSample = [ 
  [ { "valuer": "720.213", "color": 1, "$$hashKey": "object:83" }, { "valuer": "1020", "color": 1, "$$hashKey": "object:84" }, { "valuer": "540", "color": 0, "$$hashKey": "object:85" }, { "valuer": "2460", "color": 3, "$$hashKey": "object:86" }, { "valuer": "2700", "color": 3, "$$hashKey": "object:87" }, { "valuer": "2280", "color": 3, "$$hashKey": "object:88" }, { "valuer": "2640", "color": 3, "$$hashKey": "object:89" }, { "valuer": "1680", "color": 2, "$$hashKey": "object:90" }, { "valuer": "1380", "color": 1, "$$hashKey": "object:91" }, { "valuer": "960", "color": 1, "$$hashKey": "object:92" }, { "valuer": "2400", "color": 3, "$$hashKey": "object:93" }, { "valuer": "2940", "color": 4, "$$hashKey": "object:94" }, { "valuer": "720", "color": 0, "$$hashKey": "object:95" }, { "valuer": "1200", "color": 1, "$$hashKey": "object:96" }, { "valuer": "1740", "color": 2, "$$hashKey": "object:97" }, { "valuer": "1500", "color": 2, "$$hashKey": "object:98" }, { "valuer": "1500", "color": 2, "$$hashKey": "object:99" }, { "valuer": "2940", "color": 4, "$$hashKey": "object:100" }, { "valuer": "2040", "color": 2, "$$hashKey": "object:101" }, { "valuer": "3300", "color": 4, "$$hashKey": "object:102" }, { "valuer": "900", "color": 1, "$$hashKey": "object:103" }, { "valuer": "3120", "color": 4, "$$hashKey": "object:104" }, { "valuer": "2640", "color": 3, "$$hashKey": "object:105" }, { "valuer": "1200", "color": 1, "$$hashKey": "object:106" } ], [ { "valuer": "480", "color": 0, "$$hashKey": "object:155" }, { "valuer": "720", "color": 0, "$$hashKey": "object:156" }, { "valuer": "1920", "color": 2, "$$hashKey": "object:157" }, { "valuer": "3600", "color": 4, "$$hashKey": "object:158" }, { "valuer": "3540", "color": 4, "$$hashKey": "object:159" }, { "valuer": "1740", "color": 2, "$$hashKey": "object:160" }, { "valuer": "3300", "color": 4, "$$hashKey": "object:161" }, { "valuer": "1080", "color": 1, "$$hashKey": "object:162" }, { "valuer": "1500", "color": 2, "$$hashKey": "object:163" }, { "valuer": "360", "color": 0, "$$hashKey": "object:164" }, { "valuer": "1920", "color": 2, "$$hashKey": "object:165" }, { "valuer": "3300", "color": 4, "$$hashKey": "object:166" }, { "valuer": "3540", "color": 4, "$$hashKey": "object:167" }, { "valuer": "1860", "color": 2, "$$hashKey": "object:168" }, { "valuer": "1020", "color": 1, "$$hashKey": "object:169" }, { "valuer": "3000", "color": 4, "$$hashKey": "object:170" }, { "valuer": "3480", "color": 4, "$$hashKey": "object:171" }, { "valuer": "1320", "color": 1, "$$hashKey": "object:172" }, { "valuer": "3420", "color": 4, "$$hashKey": "object:173" }, { "valuer": "2760", "color": 3, "$$hashKey": "object:174" }, { "valuer": "1560", "color": 2, "$$hashKey": "object:175" }, { "valuer": "3600", "color": 4, "$$hashKey": "object:176" }, { "valuer": "780", "color": 1, "$$hashKey": "object:177" }, { "valuer": "1560", "color": 2, "$$hashKey": "object:178" } ], [ { "valuer": "1740", "color": 2, "$$hashKey": "object:227" }, { "valuer": "720", "color": 0, "$$hashKey": "object:228" }, { "valuer": "2460", "color": 3, "$$hashKey": "object:229" }, { "valuer": "1440", "color": 1, "$$hashKey": "object:230" }, { "valuer": "1380", "color": 1, "$$hashKey": "object:231" }, { "valuer": "360", "color": 0, "$$hashKey": "object:232" }, { "valuer": "3420", "color": 4, "$$hashKey": "object:233" }, { "valuer": "1080", "color": 1, "$$hashKey": "object:234" }, { "valuer": "3600", "color": 4, "$$hashKey": "object:235" }, { "valuer": "900", "color": 1, "$$hashKey": "object:236" }, { "valuer": "3540", "color": 4, "$$hashKey": "object:237" }, { "valuer": "2400", "color": 3, "$$hashKey": "object:238" }, { "valuer": "1500", "color": 2, "$$hashKey": "object:239" }, { "valuer": "1500", "color": 2, "$$hashKey": "object:240" }, { "valuer": "1680", "color": 2, "$$hashKey": "object:241" }, { "valuer": "600", "color": 0, "$$hashKey": "object:242" }, { "valuer": "1260", "color": 1, "$$hashKey": "object:243" }, { "valuer": "2460", "color": 3, "$$hashKey": "object:244" }, { "valuer": "1440", "color": 1, "$$hashKey": "object:245" }, { "valuer": "1560", "color": 2, "$$hashKey": "object:246" }, { "valuer": "720", "color": 0, "$$hashKey": "object:247" }, { "valuer": "2640", "color": 3, "$$hashKey": "object:248" }, { "valuer": "2820", "color": 3, "$$hashKey": "object:249" }, { "valuer": "1260", "color": 1, "$$hashKey": "object:250" } ], [ { "valuer": "2760", "color": 3, "$$hashKey": "object:299" }, { "valuer": "1140", "color": 1, "$$hashKey": "object:300" }, { "valuer": "1260", "color": 1, "$$hashKey": "object:301" }, { "valuer": "2160", "color": 2, "$$hashKey": "object:302" }, { "valuer": "2280", "color": 3, "$$hashKey": "object:303" }, { "valuer": "1680", "color": 2, "$$hashKey": "object:304" }, { "valuer": "3600", "color": 4, "$$hashKey": "object:305" }, { "valuer": "2400", "color": 3, "$$hashKey": "object:306" }, { "valuer": "1620", "color": 2, "$$hashKey": "object:307" }, { "valuer": "3240", "color": 4, "$$hashKey": "object:308" }, { "valuer": "1680", "color": 2, "$$hashKey": "object:309" }, { "valuer": "1500", "color": 2, "$$hashKey": "object:310" }, { "valuer": "1320", "color": 1, "$$hashKey": "object:311" }, { "valuer": "3000", "color": 4, "$$hashKey": "object:312" }, { "valuer": "2280", "color": 3, "$$hashKey": "object:313" }, { "valuer": "2940", "color": 4, "$$hashKey": "object:314" }, { "valuer": "960", "color": 1, "$$hashKey": "object:315" }, { "valuer": "1620", "color": 2, "$$hashKey": "object:316" }, { "valuer": "2940", "color": 4, "$$hashKey": "object:317" }, { "valuer": "2280", "color": 3, "$$hashKey": "object:318" }, { "valuer": "3360", "color": 4, "$$hashKey": "object:319" }, { "valuer": "2580", "color": 3, "$$hashKey": "object:320" }, { "valuer": "2520", "color": 3, "$$hashKey": "object:321" }, { "valuer": "3540", "color": 4, "$$hashKey": "object:322" } ], [ { "valuer": "420", "color": 0, "$$hashKey": "object:371" }, { "valuer": "2580", "color": 3, "$$hashKey": "object:372" }, { "valuer": "3300", "color": 4, "$$hashKey": "object:373" }, { "valuer": "3600", "color": 4, "$$hashKey": "object:374" }, { "valuer": "1920", "color": 2, "$$hashKey": "object:375" }, { "valuer": "3420", "color": 4, "$$hashKey": "object:376" }, { "valuer": "2400", "color": 3, "$$hashKey": "object:377" }, { "valuer": "2700", "color": 3, "$$hashKey": "object:378" }, { "valuer": "3360", "color": 4, "$$hashKey": "object:379" }, { "valuer": "1740", "color": 2, "$$hashKey": "object:380" }, { "valuer": "3060", "color": 4, "$$hashKey": "object:381" }, { "valuer": "300", "color": 0, "$$hashKey": "object:382" }, { "valuer": "3180", "color": 4, "$$hashKey": "object:383" }, { "valuer": "1860", "color": 2, "$$hashKey": "object:384" }, { "valuer": "2880", "color": 3, "$$hashKey": "object:385" }, { "valuer": "2460", "color": 3, "$$hashKey": "object:386" }, { "valuer": "960", "color": 1, "$$hashKey": "object:387" }, { "valuer": "600", "color": 0, "$$hashKey": "object:388" }, { "valuer": "2760", "color": 3, "$$hashKey": "object:389" }, { "valuer": "3120", "color": 4, "$$hashKey": "object:390" }, { "valuer": "3540", "color": 4, "$$hashKey": "object:391" }, { "valuer": "1140", "color": 1, "$$hashKey": "object:392" }, { "valuer": "2520", "color": 3, "$$hashKey": "object:393" }, { "valuer": "2220", "color": 3, "$$hashKey": "object:394" } ], [ { "valuer": "540", "color": 0, "$$hashKey": "object:443" }, { "valuer": "420", "color": 0, "$$hashKey": "object:444" }, { "valuer": "3480", "color": 4, "$$hashKey": "object:445" }, { "valuer": "1920", "color": 2, "$$hashKey": "object:446" }, { "valuer": "2460", "color": 3, "$$hashKey": "object:447" }, { "valuer": "3300", "color": 4, "$$hashKey": "object:448" }, { "valuer": "1680", "color": 2, "$$hashKey": "object:449" }, { "valuer": "1920", "color": 2, "$$hashKey": "object:450" }, { "valuer": "540", "color": 0, "$$hashKey": "object:451" }, { "valuer": "3300", "color": 4, "$$hashKey": "object:452" }, { "valuer": "3420", "color": 4, "$$hashKey": "object:453" }, { "valuer": "2880", "color": 3, "$$hashKey": "object:454" }, { "valuer": "2520", "color": 3, "$$hashKey": "object:455" }, { "valuer": "1020", "color": 1, "$$hashKey": "object:456" }, { "valuer": "1920", "color": 2, "$$hashKey": "object:457" }, { "valuer": "420", "color": 0, "$$hashKey": "object:458" }, { "valuer": "2280", "color": 3, "$$hashKey": "object:459" }, { "valuer": "2040", "color": 2, "$$hashKey": "object:460" }, { "valuer": "1860", "color": 2, "$$hashKey": "object:461" }, { "valuer": "3240", "color": 4, "$$hashKey": "object:462" }, { "valuer": "840", "color": 1, "$$hashKey": "object:463" }, { "valuer": "1620", "color": 2, "$$hashKey": "object:464" }, { "valuer": "2400", "color": 3, "$$hashKey": "object:465" }, { "valuer": "2460", "color": 3, "$$hashKey": "object:466" } ], [ { "valuer": "1200", "color": 1, "$$hashKey": "object:515" }, { "valuer": "600", "color": 0, "$$hashKey": "object:516" }, { "valuer": "3600", "color": 4, "$$hashKey": "object:517" }, { "valuer": "1140", "color": 1, "$$hashKey": "object:518" }, { "valuer": "1020", "color": 1, "$$hashKey": "object:519" }, { "valuer": "300", "color": 0, "$$hashKey": "object:520" }, { "valuer": "3300", "color": 4, "$$hashKey": "object:521" }, { "valuer": "1440", "color": 1, "$$hashKey": "object:522" }, { "valuer": "3000", "color": 4, "$$hashKey": "object:523" }, { "valuer": "660", "color": 0, "$$hashKey": "object:524" }, { "valuer": "2400", "color": 3, "$$hashKey": "object:525" }, { "valuer": "3360", "color": 4, "$$hashKey": "object:526" }, { "valuer": "3480", "color": 4, "$$hashKey": "object:527" }, { "valuer": "2340", "color": 3, "$$hashKey": "object:528" }, { "valuer": "1200", "color": 1, "$$hashKey": "object:529" }, { "valuer": "1140", "color": 1, "$$hashKey": "object:530" }, { "valuer": "300", "color": 0, "$$hashKey": "object:531" }, { "valuer": "2520", "color": 3, "$$hashKey": "object:532" }, { "valuer": "1500", "color": 2, "$$hashKey": "object:533" }, { "valuer": "3360", "color": 4, "$$hashKey": "object:534" }, { "valuer": "1920", "color": 2, "$$hashKey": "object:535" }, { "valuer": "960", "color": 1, "$$hashKey": "object:536" }, { "valuer": "360", "color": 0, "$$hashKey": "object:537" }, { "valuer": "900", "color": 1, "$$hashKey": "object:538" } ] ];

  var inputMatrixSample = [ "1620", "3120", "600", "420", "3420", "3360", "2340", "1080", "1260", "1020", "1020", "1680", "3480", "1440", "480", "3180", "960", "2460", "2220", "300", "2460", "480", "420", "3180", "1560", "1140", "1080", "1380", "3300", "840", "1920", "1980", "2040", "3300", "3180", "3480", "3300", "3120", "3420", "2580", "2220", "2820", "960", "2040", "3240", "1140", "2820", "2940", "3060", "3540", "960", "1020", "900", "3360", "960", "2340", "1740", "480", "1920", "2040", "960", "3600", "3300", "3120", "2340", "720", "360", "1800", "1320", "1260", "1380", "1260", "3120", "1080", "660", "540", "2280", "2400", "1080", "3600", "1320", "1680", "1080", "1200", "3240", "480", "2880", "600", "2640", "2460", "3600", "1740", "1080", "2460", "2760", "2940", "840", "900", "1200", "360", "3420", "540", "2820", "3420", "2100", "3600", "3480", "2520", "3540", "1260", "3000", "2520", "540", "420", "3180", "2100", "2400", "2400", "2640", "2580", "2280", "2400", "1440", "1560", "300", "2520", "660", "2340", "1860", "960", "1980", "480", "3060", "3000", "3600", "3240", "840", "720", "1740", "3420", "3300", "720", "1980", "960", "2640", "1800", "1980", "3600", "2220", "1560", "660", "2160", "480", "960", "1320", "480", "1560", "2940", "3300", "1680", "420", "3240", "2340", "1440", "3120", "1860", "2280", "3300" ];

  // load the directive's module
  beforeEach(module('timeheatmapApp'));

  var element, directiveElem, scope, compile;

  beforeEach(function() {
    inject(function ($compile, $rootScope) {
      compile = $compile;
      scope = $rootScope.$new();
      
      scope.chartdata = {
        "id": 1,
        "sid": 0,
        "count": 100,
        "percentage": 20
      };
      scope.timedenom = oneHourIn[0];
      scope.displaytype = displayType[0];
      scope.colorswatch = colorswatch;
      scope.matrix = scopeMatrixSample;
    });
    
  });

  function getCompiledElem() {
    var compiledDirective = compile(angular.element(templater))(scope);
    scope.$digest();
    return compiledDirective;
  }

  it('should be properly initialised', function() {
    directiveElem = getCompiledElem();

    expect(directiveElem.scope().colorswatch).toBeDefined();
    //var isolatedScope = directiveElem.scope();
    //console.log('isolatedScope', isolatedScope);
  });

  describe('augmentMatrix function', function() {
    var rowlen, columnlen, outputMatrix;

    beforeAll(function() {
      rowlen = 24;
      columnlen = 7;
      outputMatrix = augmentMatrix(inputMatrixSample, rowlen, columnlen, 3600);
    });

    it('should return a multi dimension array with m x n dimensions as specified', function() {
      expect(outputMatrix.length).toBe(columnlen);
      expect(outputMatrix[0].length).toBe(rowlen);
    });

    it('each value in the returned array should be an integer value between 0 to 4', function() {
      var isBetween0and4 = function(elemer, inder) {
        return elemer >= 0 && elemer <= 4;
      };
      outputMatrix.every(isBetween0and4);
    });
  });

  describe('rankValue function', function() {
    it('rankValue(3500,3600) should return 4', function() {
      var testValue = 3500;
      var denom = 3600/5;
      var testRankValue = Math.floor(testValue/denom);

      expect( rankValue(testValue, 3600) ).toBe( testRankValue );
    });

    it('rankValue(5200,3600) should return 4', function() {
      var testValue = 5200;
      var denom = 3600/5;

      expect( rankValue(testValue, 3600) ).toBe( 4 );
    });

    it('rankValue(0,3600) should return 0', function() {
      var testValue = 0;
      var denom = 3600/5;

      expect( rankValue(testValue, 3600) ).toBe( 0 );
    });

    it('rankValue(-120,3600) should return 0', function() {
      var testValue = -120;
      var denom = 3600/5;

      expect( rankValue(testValue, 3600) ).toBe( 0 );
    });
  });

  describe('output visual', function() {
    beforeAll(function() {
      directiveElem = getCompiledElem();
    });
    
    it('should have 7 rows', inject(function() {
      expect( directiveElem.find('.one-row').length ).toBe(7);
    }));

    it('should have 24 columns', inject(function() {
      expect( directiveElem.find('.one-row:first-child').children('span.item').length ).toBe(24);
    }));
  });

  describe('ng-show attribute', function() {
    it('should be true when scope.showlegend is true', function() {
      scope.showlegend = true;
      directiveElem = getCompiledElem();
      expect( directiveElem.find('div.legend_area').attr('ng-show') ).toBe('true');
    });

    it('should be false when scope.showlegend is false', function() {
      scope.showlegend = false;
      directiveElem = getCompiledElem();
      expect( directiveElem.find('div.legend_area').attr('ng-show') ).toBe('false');
    });

    xit('should be false when scope.showlegend is not assigned', function() {
      
      directiveElem = getCompiledElem();
      expect( directiveElem.find('div.legend_area').attr('ng-show') ).toBe('false');
    });
  });

});
