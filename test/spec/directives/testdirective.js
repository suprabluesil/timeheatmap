'use strict';

describe('Directive: testdirective', function () {

  // load the directive's module
  beforeEach(module('timeheatmapApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<testdirective></testdirective>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the testdirective directive');
  }));
});
