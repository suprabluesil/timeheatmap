'use strict';

/**
 * @ngdoc function
 * @name timeheatmapApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the timeheatmapApp
 */
angular.module('timeheatmapApp').controller('AboutCtrl', function ($scope) {

    var self = this;
    $scope.timedenomchoices = MATRIXCHARTER_HELPER.oneHourIn;
    $scope.displaytypechoices = MATRIXCHARTER_HELPER.displayType;

    /* FILE READING OPERATIONS */
    $scope.fileItem = "";
    $scope.pasteCSVText = "";

    var reader = new FileReader();
    reader.onload = function() {
    	//$scope.$apply();
    };
    $scope.filechange = function(elem) {
    	var files = elem.files;
    	//reader.readAsText(files[0]);
    	//console.log(files[0]);
    	
    	Papa.parse(files[0], {
    		header: true,
			complete: function(results) {
				self.processFile(results.data);
			}
		});
    };
    $scope.submitparse = function() {
        var results = Papa.parse($scope.pasteCSVText, {
			header: true
		});
		self.processFile(results.data);
        //event.preventDefault();
    };

    this.processFile = function(contents) {
        contents.forEach(function(elem,ind) {
            if (typeof elem.matrix === 'string') {
                elem.matrix = elem.matrix.replace(/[{}]/g, '');
                elem.matrix = elem.matrix.split(",");
            }
    	});
    	//console.log("Finished:", contents);
    	$("html, body").animate({"scrollTop":300},300);
    	self.visualize(contents);
    }

    $scope.loadsample = function() {
        $.get('./sample/testcsv_sec.csv', function(data) {
            //console.log(data);
            $scope.pasteCSVText = data;
            if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest'){
                $scope.$apply();
            }
        });
        //event.preventDefault();
    };


    /* VISUALIZING */
    $scope.timedenom = MATRIXCHARTER_HELPER.oneHourIn[0];
    $scope.displaytype = MATRIXCHARTER_HELPER.displayType[0];
    $scope.collections = [];

    this.visualize = function(matrix) {
    	$scope.collections = matrix;
        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest'){
            $scope.$apply();
        }
	}

});
