'use strict';

/**
 * @ngdoc function
 * @name timeheatmapApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the timeheatmapApp
 */
angular.module('timeheatmapApp').controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
});
