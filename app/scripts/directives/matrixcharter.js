'use strict';

var MATRIXCHARTER_HELPER = MATRIXCHARTER_HELPER || {};

var colorswatch = [
  "rgb(254,240,217)",
  "rgb(253,204,138)",
  "rgb(252,141,89)",
  "rgb(227,74,51)",
  "rgb(179,0,0)"
];
var oneHourIn = [
  { keyer:"seconds", valuer:3600 },
  { keyer:"minutes", valuer:60 }
];
var displayType = [
  { keyer:"weekly", valuer:[24,7] },
  { keyer:"monthly", valuer:[7,5] }
];

MATRIXCHARTER_HELPER.colorswatch = colorswatch.slice(0);
MATRIXCHARTER_HELPER.oneHourIn = oneHourIn.slice(0);
MATRIXCHARTER_HELPER.displayType = displayType.slice(0);

var rankValue = function(valuer, onehouris) {
	//console.log('check onehouris', onehouris);
	var denom = onehouris / colorswatch.length;
	var ranker = 0;
	if (valuer > onehouris) {
		return colorswatch.length - 1;
	} else {
		for (var i = denom; i <= onehouris; i=i+denom) {
			if (valuer > i) {
				ranker++;
			}
		}
		return ranker;
	}
};
var augmentMatrix = function(inputmatrix, rowlength, collength, onehouris) {
	var resultmatrix = [];
    for (var i = 0; i < collength; i++) {
    	var temp_arr = [];
    	var temp_num = 0;
    	for (var j = 0; j < rowlength; j++) {
    		temp_num = inputmatrix[ rowlength*i +j ];
    		temp_arr.push({
    			'valuer': temp_num,
    			'color': rankValue(temp_num, onehouris)
    		});
    	}
    	resultmatrix.push(temp_arr);
    }
	return resultmatrix;
};

/**
 * @ngdoc directive
 * @name timeheatmapApp.directive:matrixcharter
 * @description
 * # matrixcharter
 */
angular.module('timeheatmapApp').directive('matrixcharter', function () {
    return {
      templateUrl: 'views/dirtemplates/matrixcharter.html',
      restrict: 'E',
      replace: true,
      scope: {
      	chartdata: '=',
      	timedenom: '=',
      	displaytype: '=',
      	showlegend: '@'
      },
      link: function postLink(scope, element, attrs) {
      	scope.colorswatch = colorswatch;
      	//onehouris = scope.timedenom.valuer;
	    scope.matrix = augmentMatrix(scope.chartdata.matrix, 
	    							scope.displaytype.valuer[0], 
	    							scope.displaytype.valuer[1], 
	    							scope.timedenom.valuer);
	    //console.log('check in directive', scope.timedenom, scope.matrix);
      }
    };
});
