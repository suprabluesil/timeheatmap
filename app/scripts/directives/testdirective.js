'use strict';

/**
 * @ngdoc directive
 * @name timeheatmapApp.directive:testdirective
 * @description
 * # testdirective
 */
angular.module('timeheatmapApp')
  .directive('testdirective', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the testdirective directive');
      }
    };
  });
