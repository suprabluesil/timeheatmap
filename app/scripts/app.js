'use strict';

/**
 * @ngdoc overview
 * @name timeheatmapApp
 * @description
 * # timeheatmapApp
 *
 * Main module of the application.
 */
var timeheatmapApp = angular.module('timeheatmapApp', [
    'ngAnimate','ngCookies','ngResource','ngRoute','ngSanitize','ngTouch'
]);

timeheatmapApp.config(function ($routeProvider) {
  $routeProvider
    /*.when('/main', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl',
      controllerAs: 'main'
    })*/
    .when('/', {
      templateUrl: 'views/about.html',
      controller: 'AboutCtrl',
      controllerAs: 'about'
    })
    .otherwise({
      redirectTo: '/'
    });
});